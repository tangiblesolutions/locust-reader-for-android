package com.tangible.locustreader.tools;

import android.util.Log;

import java.io.File;

import uk.co.senab.bitmapcache.BitmapLruCache;

public class ImageCache {

    static BitmapLruCache cache;

    static{
        File diskCacheLocation = MyApplication.getAppContext().getExternalFilesDir(null);
        BitmapLruCache.Builder builder = new BitmapLruCache.Builder();
        builder.setMemoryCacheEnabled(true).setMemoryCacheMaxSizeUsingHeapSize();
        builder.setDiskCacheEnabled(true).setDiskCacheLocation(diskCacheLocation);
        cache = builder.build();
        Log.i("IC", "INITIALIZED IMAGE CACHE!");
    }

    public static BitmapLruCache getCacheInstance() {
        return cache;
    }
}
