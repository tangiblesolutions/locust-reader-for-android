package com.tangible.locustreader.tools;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class ArticleCacheManager extends SQLiteOpenHelper {

    public static final String DB_NAME = "locustdb";
    public static final String MASTER_ARTICLE_LIST = "master_article_list";
    public static final String IGNORED_ARTICLE_LIST = "ignored_article_list";
    public static final String SAVED_ARTICLE_LIST = "saved_article_list";

    public ArticleCacheManager(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createMasterArticleList = "CREATE TABLE " + MASTER_ARTICLE_LIST + " (id VARCHAR(20) PRIMARY KEY)";
        String createIgnoredArticleList = "CREATE TABLE " + IGNORED_ARTICLE_LIST + " (id VARCHAR(20) PRIMARY KEY)";
        String createSavedArticleList = "CREATE TABLE " + SAVED_ARTICLE_LIST + " (id VARCHAR(20) PRIMARY KEY)";

        db.execSQL(createMasterArticleList);
        db.execSQL(createIgnoredArticleList);
        db.execSQL(createSavedArticleList);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void cacheMasterArticleList(ArrayList<String> ids) {
        long currentTime = System.currentTimeMillis();
        clearMasterArticleList();

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Log.i("ACM", "Caching " + ids.size() + " articles...");
        while (ids.size() > 0) {
            List<String> sublist = new ArrayList<>();
            if (ids.size() >= 20) { //section into subpieces of 20
                sublist.addAll(ids.subList(0, 20)); //upper bound is exclusive!
            } else {
                sublist.addAll(ids);
            }

            sqLiteDatabase.beginTransaction();
            for (String id : sublist) {
                sqLiteDatabase.execSQL("INSERT INTO " + MASTER_ARTICLE_LIST + " (id) VALUES ('" + id + "')");
            }
            sqLiteDatabase.setTransactionSuccessful();
            sqLiteDatabase.endTransaction();
            ids.removeAll(sublist);
        }

        Log.i("ACM", "Finished Article Caching in " + (System.currentTimeMillis() - currentTime) + "ms");
    }

    public ArrayList<String> getMasterArticleList() {
        ArrayList<String> articlesToReturn = new ArrayList<>();

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + MASTER_ARTICLE_LIST, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            Log.i("ACM", cursor.getCount() + " articles in Master Article Cache");
            while (cursor.moveToNext()) {
                articlesToReturn.add(cursor.getString(cursor.getColumnIndex("id")));
            }
            cursor.close();
        } else {
            Log.i("ACM", "Cursor null or count zero!");
        }

        return articlesToReturn;
    }

    private void clearMasterArticleList() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("DELETE FROM " + MASTER_ARTICLE_LIST);
    }

    public ArrayList<String> get20ArticlesToShowNext() {
        ArrayList<String> masterArticleList = getMasterArticleList();
        ArrayList<String> articlesToReturn = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        if (masterArticleList.size() >= 20) {
            for (int i = 0; i < 20; i++) {
                articlesToReturn.add(masterArticleList.get(i));
            }
        } else {
            for (int i = 0; i < masterArticleList.size(); i++) { //empty flag after this finishes!
                articlesToReturn.add(masterArticleList.get(i));
            }
        }

//        for (String x : articlesToReturn) {
//            masterArticleList.remove(x);
//            sqLiteDatabase.delete(MASTER_ARTICLE_LIST, "id=?", );
//        }

        sqLiteDatabase.delete(MASTER_ARTICLE_LIST, generateOrStatementForIDList(articlesToReturn, "id"), articlesToReturn.toArray(new String[articlesToReturn.size()]));

        return articlesToReturn;
    }

    public void saveArticle(String id) { //metadata to come later!
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("INSERT INTO " + SAVED_ARTICLE_LIST + " (id) VALUES ('" + id + "')");
    }

    public void removeSavedArticle(String id) { //metadata to come later!
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.rawQuery("DELETE FROM " + SAVED_ARTICLE_LIST + " WHERE id=?", new String[]{id});
    }

    public boolean isArticleSaved(String id) {
        boolean status = false;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + SAVED_ARTICLE_LIST + " WHERE id=?", new String[]{id});
        if (cursor != null && cursor.getCount() > 0) {
            status = true;
            cursor.close();
        }
        return status;
    }

    private String generateOrStatementForIDList(ArrayList<String> ids, String fieldName) {
        String or = "(";

        for (int i = 0; i < ids.size(); i++) {

            if (i == ids.size() - 1) {
                or += (fieldName + "=?");
            } else {
                or += (fieldName + "=? OR ");
            }
        }
        or += ")";

        return or;
    }
}


