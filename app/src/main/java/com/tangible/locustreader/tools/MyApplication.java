package com.tangible.locustreader.tools;

import android.app.Application;
import android.content.Context;

import java.util.Date;

public class MyApplication extends Application {

    private static Context context;
    private static long cacheLastInvalidated;

    public void onCreate() {
        super.onCreate();
        MyApplication.context = getApplicationContext();
        cacheLastInvalidated = new Date().getTime();
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }

    public static void cacheWasJustInvalidated() {
        cacheLastInvalidated = new Date().getTime();
    }

    public static long getDeltaCacheLastInvalidated() {
        return new Date().getTime() - cacheLastInvalidated;
    }
}
