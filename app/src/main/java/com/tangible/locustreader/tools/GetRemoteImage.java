package com.tangible.locustreader.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.net.URL;

import uk.co.senab.bitmapcache.BitmapLruCache;


public class GetRemoteImage extends AsyncTask<String, Integer, Bitmap> {

    //NEEDS TO HANDLE EXCEPTIONS!!!!


    Bitmap picture;
    ImageView imageView;
    Context context;

    public GetRemoteImage(Context context, ImageView imageView) {
        this.imageView = imageView;
        this.context = context;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        if (params[0] != null) {
            BitmapLruCache cache = ImageCache.getCacheInstance();
            if (cache.contains(params[0])) {
                return cache.get(params[0]).getBitmap();
            }

            URL imageURL;
            try {
                imageURL = new URL(params[0]);
                picture = BitmapFactory.decodeStream(imageURL.openStream());
                cache.put(params[0], picture, Bitmap.CompressFormat.JPEG, 45);
                return picture;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return BitmapFactory.decodeResource(context.getResources(), 0);
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);

    }


}
