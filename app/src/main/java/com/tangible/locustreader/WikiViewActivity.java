package com.tangible.locustreader;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.tangible.locustreader.api.parse.APIResponse;
import com.tangible.locustreader.api.parse.Article;
import com.tangible.locustreader.tools.ArticleCacheManager;

import org.apache.http.Header;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class WikiViewActivity extends AppCompatActivity {

    String currentPageID = "";
    boolean isCurrentPageSaveable = false;

    WebView wikiWebView;
    ArticleCacheManager articleCacheManager;
    MenuItem saveIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wiki_view);
        articleCacheManager = new ArticleCacheManager(this);

        String wikiPageID = getIntent().getExtras().getString("currentPageID");

        wikiWebView = (WebView) findViewById(R.id.wikiWebView);
        wikiWebView.getSettings().setDomStorageEnabled(true);
        wikiWebView.getSettings().setJavaScriptEnabled(true);
        //wikiWebView.setWebChromeClient(new MyWebChromeClient());
        wikiWebView.setWebViewClient(new MyWebViewClient());
        wikiWebView.loadUrl("http://en.m.wikipedia.org/?curid=" + wikiPageID);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_wiki_view, menu);
        saveIcon = menu.findItem(R.id.save_button);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.save_button && isCurrentPageSaveable) {
            articleCacheManager.saveArticle(currentPageID);
            fadeOutMenuItem(saveIcon);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {


            Log.i("WVA", "WIKIVIEW URL: " + url);

            if (url.contains("wikipedia.org/?curid=") || url.contains("wikipedia.org/wiki/")) {

                if (url.contains("wikipedia.org/wiki/")) {//get ID from title
                    String title = url.substring(url.indexOf("wiki/") + 5);
                    String urlToGetID = "https://en.wikipedia.org/w/api.php?action=query&prop=pageprops&format=json&titles=" + title;
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.post(urlToGetID, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            Gson gson = new GsonBuilder().create();
                            try {
                                APIResponse apiResponse = gson.fromJson(new StringReader(new String(responseBody, "UTF-8")), APIResponse.class);
                                currentPageID = ((ArrayList<Article>) apiResponse.getQuery().getPages().values()).get(0).getPageid();
                                decideIfSaveable(currentPageID);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                        }
                    });
                } else if (url.contains("=")) {//get ID from title
                    currentPageID = url.substring(url.indexOf("=") + 1);
                    decideIfSaveable(currentPageID);
                }

            } else { //page isnt saveable!

            }

            view.loadUrl(url);
            return false;
        }
    }

    private void decideIfSaveable(String pageID) {
        if (currentPageID != null) {
            if (articleCacheManager.isArticleSaved(currentPageID)) { //set highlited icon and set savable bool
                if (isCurrentPageSaveable) {
                    isCurrentPageSaveable = false;
                    return; //were good already
                }
                fadeOutMenuItem(saveIcon);
                saveIcon.setIcon(R.drawable.abc_btn_rating_star_off_mtrl_alpha_orange);
                fadeInMenuItem(saveIcon);
                isCurrentPageSaveable = true;
            } else { //set non highlighted icon

            }
        }
    }

    private void fadeOutMenuItem(MenuItem menuItem) {
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator objectAnimatorX = ObjectAnimator.ofFloat(menuItem, "scaleX", 1.0f, 0.0f);
        objectAnimatorX.setDuration(100);

        ObjectAnimator objectAnimatorY = ObjectAnimator.ofFloat(menuItem, "scaleY", 1.0f, 0.0f);
        objectAnimatorY.setDuration(100);
        animatorSet.playTogether(objectAnimatorX, objectAnimatorY);
        animatorSet.start();
    }

    private void fadeInMenuItem(MenuItem menuItem) {
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator objectAnimatorX = ObjectAnimator.ofFloat(menuItem, "scaleX", 0, 1);
        objectAnimatorX.setDuration(100);

        ObjectAnimator objectAnimatorY = ObjectAnimator.ofFloat(menuItem, "scaleY", 0, 1);
        objectAnimatorY.setDuration(100);
        animatorSet.playTogether(objectAnimatorX, objectAnimatorY);
        animatorSet.start();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && wikiWebView.canGoBack()) {
            wikiWebView.goBack();
            return true;
        } else {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }


}


