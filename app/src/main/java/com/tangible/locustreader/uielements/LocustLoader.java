package com.tangible.locustreader.uielements;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tangible.locustreader.R;
import com.tangible.locustreader.api.QueryBuilder;
import com.tangible.locustreader.api.parse.Article;
import com.tangible.locustreader.interfaces.OnLocustChangeListener;

import java.util.ArrayList;

public class LocustLoader extends RelativeLayout implements OnLocustChangeListener {

    TextView statusMessage, percentage;
    ImageView rightWing, leftWing;
    AnimatorSet upDownTotalSet;

    //config
    long flapDuration = 75;
    long shortFlapDuration = 60;


    float flatScale = 1.0f;
    float upScale = 0.3f;
    float downScale = 0.65f;

    public LocustLoader(Context context) {
        super(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        layoutInflater.inflate(R.layout.locustloader, this);
        rightWing = (ImageView) findViewById(R.id.rightWing);
        leftWing = (ImageView) findViewById(R.id.leftWing);
        statusMessage = (TextView) findViewById(R.id.statusText);
        percentage = (TextView) findViewById(R.id.percentage);

        //beginFlyingAnimation();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        beginFlyingAnimation();
    }

    private void beginFlyingAnimation() {

        leftWing.setPivotX(leftWing.getWidth());
        rightWing.setPivotX(0);


        //up to top from flat
        AnimatorSet topFromFlat = new AnimatorSet();

        ObjectAnimator upRight = ObjectAnimator.ofFloat(rightWing, "scaleX", flatScale, upScale);
        upRight.setDuration(flapDuration);

        ObjectAnimator upLeft = ObjectAnimator.ofFloat(leftWing, "scaleX", flatScale, upScale);
        upLeft.setDuration(flapDuration);

        topFromFlat.playTogether(upLeft, upRight);


        //down to flat from top
        AnimatorSet flatFromTop = new AnimatorSet();
        ObjectAnimator downRight = ObjectAnimator.ofFloat(rightWing, "scaleX", upScale, flatScale);
        downRight.setDuration(flapDuration);

        ObjectAnimator downLeft = ObjectAnimator.ofFloat(leftWing, "scaleX", upScale, flatScale);
        downLeft.setDuration(flapDuration);

        flatFromTop.playTogether(downLeft, downRight);


        //to down from flat
        AnimatorSet downFromFlat = new AnimatorSet();
        ObjectAnimator downFromFlatRight = ObjectAnimator.ofFloat(rightWing, "scaleX", flatScale, downScale);
        downFromFlatRight.setDuration(shortFlapDuration);

        ObjectAnimator downFromFlatLeft = ObjectAnimator.ofFloat(leftWing, "scaleX", flatScale, downScale);
        downFromFlatLeft.setDuration(shortFlapDuration);

        downFromFlat.playTogether(downFromFlatLeft, downFromFlatRight);
        //downFromFlat.setInterpolator(new DecelerateInterpolator());

        //to flat from down
        AnimatorSet flatFromDown = new AnimatorSet();
        ObjectAnimator flatFromDownRight = ObjectAnimator.ofFloat(rightWing, "scaleX", downScale, flatScale);
        flatFromDownRight.setDuration(shortFlapDuration);

        ObjectAnimator flatFromDownLeft = ObjectAnimator.ofFloat(leftWing, "scaleX", downScale, flatScale);
        flatFromDownLeft.setDuration(shortFlapDuration);

        flatFromDown.playTogether(flatFromDownRight, flatFromDownLeft);
        //flatFromDown.setInterpolator(new DecelerateInterpolator());

        upDownTotalSet = new AnimatorSet();
        upDownTotalSet.playSequentially(topFromFlat, flatFromTop, downFromFlat, flatFromDown); //
        upDownTotalSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                //Log.i("LL", "ANIM START");
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //Log.i("LL", "ANIM END");
                upDownTotalSet.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });

        upDownTotalSet.start(); //start the set
    }

    @Override
    public void onLayoutComplete(ArrayList<Article> articles) {
    }

    @Override
    public void onLayoutStatusChange(int status) {
        switch (status) {
            case QueryBuilder.REFRESHING_MASTER_LIST: {
                this.setVisibility(VISIBLE);
                //statusMessage.setText("Loading");
                break;
            }
            case QueryBuilder.GETTING_ARTICLES: {
                //statusMessage.setText("Refreshing Articles");
                break;
            }
            case QueryBuilder.RENDERING_ARTICLES: {
               // statusMessage.setText("Rendering");
                break;
            }
            case QueryBuilder.COMPLETE: {
                //percentage.setText("");
                this.setVisibility(GONE);
                break;
            }
            case QueryBuilder.INVALIDATED: {
                this.setVisibility(GONE);
                break;
            }
        }
    }

    @Override
    public void onProgressPercentChange(String percentageToShow) {
        percentage.setText(percentageToShow);
    }
}
