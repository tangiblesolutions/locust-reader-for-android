package com.tangible.locustreader.uielements;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.tangible.locustreader.R;
import com.tangible.locustreader.WikiViewActivity;
import com.tangible.locustreader.api.parse.Article;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ConvertViewHolder> {
    private Context mContext;
    LayoutInflater layoutInflater;
    ArrayList<Article> articles;


    //article display stuff
    DisplayImageOptions imageOptions = new DisplayImageOptions.Builder().displayer(new FadeInBitmapDisplayer(200)).cacheInMemory(true).build();
    ImageLoader imageLoader = ImageLoader.getInstance();

    public RecyclerViewAdapter(Context context, @Nullable ArrayList<Article> articles) {
        if (articles != null) {
            this.articles = articles;
        } else {
            this.articles = new ArrayList<>();
        }

        this.mContext = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ConvertViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ConvertViewHolder(layoutInflater.inflate(R.layout.articlelistitem, parent, false));
    }

    @Override
    public void onBindViewHolder(ConvertViewHolder holder, int position) {
        Article article = articles.get(position);
        if (article.getThumbnail() != null) {
            imageLoader.displayImage(article.getThumbnail().getOriginal(), holder.backgroundImage, imageOptions);
            //new GetRemoteImage(mContext, holder.backgroundImage).execute(article.getThumbnail().getOriginal());
        } else {
            //default image and notify layout complete listener
        }
        holder.title.setText(article.getTitle());
        holder.titleShadow.setText(article.getTitle());
        holder.distance.setText(String.valueOf(article.getCoordinates().get(0).getDist()) + "m");
        holder.distanceShadow.setText(String.valueOf(article.getCoordinates().get(0).getDist()) + "m");
        holder.description.setText(article.getExtract());
        holder.imageView.bringToFront();
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }


    public class ConvertViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        SelectableRoundedImageView backgroundImage;
        ImageView imageView;

        TextView title, titleShadow, distance, distanceShadow, description;

        public ConvertViewHolder(View view) {
            super(view);
            backgroundImage = (SelectableRoundedImageView) view.findViewById(R.id.backgroundPicture);
            title = (TextView) view.findViewById(R.id.title);
            titleShadow = (TextView) view.findViewById(R.id.titleShadow);
            description = (TextView) view.findViewById(R.id.description);
            distance = (TextView) view.findViewById(R.id.distance);
            distanceShadow = (TextView) view.findViewById(R.id.distanceShadow);
            imageView = (ImageView) view.findViewById(R.id.imageView7);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Activity activity = (Activity) mContext;
            Intent intent = new Intent(activity, WikiViewActivity.class);
            intent.putExtra("pageID", articles.get(getAdapterPosition()).getPageid());
            activity.startActivity(intent);
        }
    }
}

