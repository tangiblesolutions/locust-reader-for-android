package com.tangible.locustreader.uielements;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tangible.locustreader.R;
import com.tangible.locustreader.WikiViewActivity;
import com.tangible.locustreader.api.parse.Article;

public class ArticleListItem extends RelativeLayout {

    Article thisArticle;
    CardView cardView;
    SelectableRoundedImageView backgroundImage;

    TextView title, titleShadow, distance, distanceShadow, description;

    public ArticleListItem(Context context) {
        super(context);
    }

    public ArticleListItem(Context context, final Article article) {
        super(context);
        thisArticle = article;

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        layoutInflater.inflate(R.layout.articlelistitem, this);

        //layout elements
        cardView = (CardView) findViewById(R.id.card_view);
        cardView.setPreventCornerOverlap(false);

        backgroundImage = (SelectableRoundedImageView) findViewById(R.id.backgroundPicture);
        title = (TextView) findViewById(R.id.title);
        titleShadow = (TextView) findViewById(R.id.titleShadow);
        description = (TextView) findViewById(R.id.description);
        distance = (TextView) findViewById(R.id.distance);
        distanceShadow = (TextView) findViewById(R.id.distanceShadow);

        //background image
        DisplayImageOptions imageOptions = new DisplayImageOptions.Builder().displayer(new FadeInBitmapDisplayer(200)).build();
        ImageLoader imageLoader = ImageLoader.getInstance();

        if (article.getThumbnail() != null) {
            imageLoader.displayImage(article.getThumbnail().getOriginal(), backgroundImage, imageOptions);
        } else {
            //default image and notify layout complete listener
        }
        imageLoader.setDefaultLoadingListener(new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                //layout complete listener here
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });

        title.setText(article.getTitle());
        titleShadow.setText(article.getTitle());

        distance.setText(String.valueOf(article.getCoordinates().get(0).getDist()) + "m");
        distanceShadow.setText(String.valueOf(article.getCoordinates().get(0).getDist()) + "m");

        description.setText(article.getExtract());

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) { //show webview article
                Activity activity = (Activity) getContext();
                Intent intent = new Intent(activity, WikiViewActivity.class);
                intent.putExtra("pageID", thisArticle.getPageid());
                activity.startActivity(intent);
            }
        });

    }


}
