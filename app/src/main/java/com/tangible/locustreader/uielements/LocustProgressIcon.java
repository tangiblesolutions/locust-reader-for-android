package com.tangible.locustreader.uielements;

import android.content.Context;
import android.widget.RelativeLayout;

import com.tangible.locustreader.api.QueryBuilder;

public class LocustProgressIcon extends RelativeLayout {
    public LocustProgressIcon(Context context) {
        super(context);
    }

    public void onProgressUpdate(int progressCode) {
        switch (progressCode) {
            case QueryBuilder.GETTING_ARTICLES: {

                break;
            }
            case QueryBuilder.REFRESHING_MASTER_LIST: {

                break;
            }
            case QueryBuilder.RENDERING_ARTICLES: {

                break;
            }
        }
    }

}
