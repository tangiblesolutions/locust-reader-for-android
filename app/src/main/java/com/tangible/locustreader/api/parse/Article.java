package com.tangible.locustreader.api.parse;

import java.util.ArrayList;

public class Article {
    String pageid;
    String ns;
    String title;
    ArrayList<CoordinateSet> coordinates;
    String extract;
    Thumbnail thumbnail;

    public String getPageid() {
        return pageid;
    }

    public String getNs() {
        return ns;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<CoordinateSet> getCoordinates() {
        return coordinates;
    }

    public String getExtract() {
        return extract;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setExtract(String extract) {
        this.extract = extract;
    }
}
