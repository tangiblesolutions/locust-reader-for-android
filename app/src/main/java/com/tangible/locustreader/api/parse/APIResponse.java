package com.tangible.locustreader.api.parse;

import java.util.ArrayList;
import java.util.HashMap;

public class APIResponse {
    Query query;
    String batchcomplete;

    public Query getQuery() {
        return query;
    }

    public String getBatchcomplete() {
        return batchcomplete;
    }

    public class Query {
        HashMap<String, Article> pages;
        ArrayList<Article> geosearch;

        public HashMap<String, Article> getPages() {
            return pages;
        }

        public ArrayList<Article> getGeosearch() {
            return geosearch;
        }
    }


}
