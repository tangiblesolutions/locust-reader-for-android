package com.tangible.locustreader.api.parse;

public class CoordinateSet {
    double lat, lon, dist;
    String primary;

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getPrimary() {
        return primary;
    }

    public double getDist() {
        return dist;
    }
}
