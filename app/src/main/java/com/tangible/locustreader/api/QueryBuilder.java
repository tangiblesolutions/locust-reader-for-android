package com.tangible.locustreader.api;

import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.tangible.locustreader.HomeActivity;
import com.tangible.locustreader.api.parse.APIResponse;
import com.tangible.locustreader.api.parse.Article;
import com.tangible.locustreader.tools.ArticleCacheManager;

import org.apache.http.Header;

import java.io.StringReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class QueryBuilder {
    HomeActivity homeActivity;

    //constants
    public static final int REFRESHING_MASTER_LIST = 0;
    public static final int GETTING_ARTICLES = 1;
    public static final int RENDERING_ARTICLES = 2;
    public static final int COMPLETE = 3;
    public static final int INVALIDATED = 4;
    public static final int FAILURE = 5;

    ArticleCacheManager articleCacheManager;


    public QueryBuilder(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
        articleCacheManager = new ArticleCacheManager(homeActivity);
    }

    public void doInitialRefresh(double x, double y) {
        Log.i("QB", "INITREFRESH 1");
        refreshMasterArticleList(x, y, true);
    }

    public void refreshMasterArticleList(final double x, final double y, final boolean init) {
        homeActivity.onLayoutStatusChange(INVALIDATED);
        homeActivity.onLayoutStatusChange(REFRESHING_MASTER_LIST);
        final String gscoord = String.valueOf(y) + "|" + String.valueOf(x);

        Map<String, String> pairList = new HashMap<>();
        pairList.put("action", "query");
        pairList.put("list", "geosearch");
        pairList.put("format", "json");
        pairList.put("gscoord", gscoord);
        pairList.put("gsradius", "5000");
        pairList.put("gslimit", "200");

        final String url = "https://en.wikipedia.org/w/api.php" + createQueryStringForParameters(pairList);
        Log.i("QB", "To Wikipedia: " + url);
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(homeActivity, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    Gson gson = new GsonBuilder().create();
                    APIResponse apiResponse = gson.fromJson(new StringReader(new String(responseBody, "UTF-8")), APIResponse.class);

                    ArrayList<String> articleIDS = new ArrayList<>();
                    for (Article article : apiResponse.getQuery().getGeosearch()) {
                        articleIDS.add(article.getPageid());
                    }

                    Log.i("QB", "Recieved " + articleIDS.size() + " article IDS from Wikipedia!");

                    articleCacheManager.cacheMasterArticleList(articleIDS);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                //begin secondary
                if (init) getArticlesForIDSWithDistances(null, x, y);
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
                String percentageComplete = (Math.round((bytesWritten / totalSize) / 4)) + "%";
                homeActivity.onProgressPercentChange(percentageComplete);
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i("QB", "Query failed with HTTP status code " + statusCode + "\nURL: " + url);
                error.printStackTrace();
            }
        });
    }


    public void getArticlesForIDSWithDistances(@Nullable ArrayList<String> ids, double x, double y) {


        final String gscoord = String.valueOf(y) + "|" + String.valueOf(x);

        ArrayList<String> articleIDS;
        if (ids != null) {
            articleIDS = ids;
        } else {
            articleIDS = articleCacheManager.get20ArticlesToShowNext();

            if (articleIDS.size() < 20) { //refresh master list with exclusion?
                Log.e("QB", "WARNING!!! ACM.get20ArticlesToShowNext only returned " + articleIDS.size() + " articles!");
            }
        }

        String pageIDS = "";
        for (int i = 0; i < articleIDS.size(); i++) {
            if (i == articleIDS.size() - 1) {
                pageIDS += articleIDS.get(i);
            } else {
                pageIDS += (articleIDS.get(i) + "|");
            }
        }

        HashMap<String, String> pairList = new HashMap<>();
        pairList.put("action", "query");
        pairList.put("prop", "coordinates|extracts|pageimages");
        pairList.put("format", "json");
        pairList.put("codistancefrompoint", gscoord);
        pairList.put("colimit", "20");
        pairList.put("exsentences", "3");
        pairList.put("exlimit", "20");
        pairList.put("piprop", "original");
        pairList.put("pilimit", "20");
        pairList.put("pageids", pageIDS);
        pairList.put("exintro", "");
        pairList.put("explaintext", "");

        final String url = "https://en.wikipedia.org/w/api.php" + createQueryStringForParameters(pairList);

        Log.i("QB", "To Wikipedia: " + url);

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(homeActivity, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    Gson gson = new GsonBuilder().create();
                    APIResponse apiResponse = gson.fromJson(new StringReader(new String(responseBody, "UTF-8")), APIResponse.class);
                    ArrayList<Article> articles = new ArrayList<>(apiResponse.getQuery().getPages().values());
                    Log.i("QB", "Recieved " + articles.size() + " Article previews from Wikipedia!");
                    homeActivity.onLayoutComplete(articles);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                //finish callback
            }

            @Override
            public void onStart() {
                Log.i("QB", "Started GetArticlePreviews");
                super.onStart();
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
                String percentageComplete = (Math.round((bytesWritten / totalSize) / 4) * 100) + "%";
                homeActivity.onProgressPercentChange(percentageComplete);
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i("QB", "Query failed with HTTP status code " + statusCode + "\nURL: " + url);
                error.printStackTrace();
            }
        });


    }

    private static final char PARAMETER_DELIMITER = '&';
    private static final char PARAMETER_EQUALS_CHAR = '=';

    public String createQueryStringForParameters(Map<String, String> parameters) {
        StringBuilder parametersAsQueryString = new StringBuilder();
        if (parameters != null) {
            boolean firstParameter = true;

            for (String parameterName : parameters.keySet()) {
                if (!firstParameter) {
                    parametersAsQueryString.append(PARAMETER_DELIMITER);
                } else {
                    parametersAsQueryString.append("?");
                }

                if (parameterName == null) Log.i("QB", "PARAMETER NAME NULL!");

                parametersAsQueryString.append(parameterName)
                        .append(PARAMETER_EQUALS_CHAR)
                        .append(URLEncoder.encode(
                                parameters.get(parameterName)));


                firstParameter = false;
            }
        }
        return parametersAsQueryString.toString();
    }
}
