package com.tangible.locustreader.interfaces;

import com.tangible.locustreader.api.parse.Article;

import java.util.ArrayList;

public interface OnLocustChangeListener {
    void onLayoutComplete(ArrayList<Article> articles);

    void onLayoutStatusChange(int status);

    void onProgressPercentChange(String percentageToShow);
}
