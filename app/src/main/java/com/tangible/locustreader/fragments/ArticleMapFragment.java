package com.tangible.locustreader.fragments;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.GoogleMap;
import com.tangible.locustreader.HomeActivity;
import com.tangible.locustreader.R;
import com.tangible.locustreader.api.QueryBuilder;
import com.tangible.locustreader.api.parse.Article;
import com.tangible.locustreader.interfaces.OnLocustChangeListener;
import com.tangible.locustreader.uielements.RecyclerViewAdapter;

import java.util.ArrayList;

public class ArticleMapFragment extends Fragment implements OnLocustChangeListener {

    RelativeLayout messageBarLayout;
    RecyclerView articleList;
    FragmentManager fragmentManager;
    GoogleMap articleMap;
    Location location;

    Context context;

    boolean isFirstLoad = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_article_map, container, false);
        context = v.getContext();

        fragmentManager = ((HomeActivity) context).getSupportFragmentManager();

        articleList = (RecyclerView) v.findViewById(R.id.articleList);
        articleList.setAdapter(new RecyclerViewAdapter(context, null));
        articleList.setLayoutManager(new LinearLayoutManager(context));

        messageBarLayout = (RelativeLayout) v.findViewById(R.id.messageBarLayout);
        //articleMap = ((SupportMapFragment) fragmentManager.findFragmentById(R.id.articleMap)).getMap();

        if (articleMap == null) {
            Log.i("AMF", "Google Map Null!");
        }

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public void onLayoutStatusChange(int status) {
        switch (status) {
            case QueryBuilder.REFRESHING_MASTER_LIST: {

                break;
            }
            case QueryBuilder.GETTING_ARTICLES: {

                break;
            }
            case QueryBuilder.RENDERING_ARTICLES: {

                break;
            }
            case QueryBuilder.COMPLETE: {

                break;
            }
            case QueryBuilder.INVALIDATED: {

                break;
            }
        }
    }

    @Override
    public void onProgressPercentChange(String percentageToShow) {

    }

    @Override
    public void onLayoutComplete(ArrayList<Article> articles) {
        location = ((HomeActivity) context).getLocationAtTimeOfQuery();
        articleList.setAdapter(new RecyclerViewAdapter(context, articles));

        /*CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

        articleMap.moveCamera(center);
        articleMap.animateCamera(zoom); //should be determined programmatically with bounds

        //set markers
        for (Article article : articles) {
            if (article.getCoordinates().get(0) != null) {
                MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(article.getCoordinates().get(0).getLat(), article.getCoordinates().get(0).getLat()));
                articleMap.addMarker(markerOptions);
            }
        }*/


    }


}
