package com.tangible.locustreader.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.tangible.locustreader.R;
import com.tangible.locustreader.api.parse.Article;
import com.tangible.locustreader.interfaces.OnLocustChangeListener;
import com.tangible.locustreader.uielements.LocustLoader;
import com.tangible.locustreader.uielements.RecyclerViewAdapter;

import java.util.ArrayList;

public class HomeFragment extends Fragment implements OnLocustChangeListener {

    RelativeLayout messageBarLayout, rootLayout;
    RecyclerView articleList;
    Context context;

    LocustLoader locustLoader;

    boolean isFirstLoad = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_home, container, false);
        context = v.getContext();

        articleList = (RecyclerView) v.findViewById(R.id.list);
        articleList.setLayoutManager(new LinearLayoutManager(context));
        articleList.setAdapter(new RecyclerViewAdapter(context, null));

        messageBarLayout = (RelativeLayout) v.findViewById(R.id.messageBarLayout);
        rootLayout = (RelativeLayout) v.findViewById(R.id.root);

        locustLoader = new LocustLoader(context);
        rootLayout.addView(locustLoader);

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //loading animation
    }


    @Override
    public void onLayoutComplete(ArrayList<Article> articles) {
        articleList.setAdapter(new RecyclerViewAdapter(context, articles));
    }

    @Override
    public void onLayoutStatusChange(int status) {
        locustLoader.onLayoutStatusChange(status);
    }

    @Override
    public void onProgressPercentChange(String percentageToShow) {
        locustLoader.onProgressPercentChange(percentageToShow);
    }

}
