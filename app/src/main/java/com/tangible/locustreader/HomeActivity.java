package com.tangible.locustreader;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.tangible.locustreader.api.QueryBuilder;
import com.tangible.locustreader.api.parse.Article;
import com.tangible.locustreader.fragments.ArticleMapFragment;
import com.tangible.locustreader.fragments.HomeFragment;
import com.tangible.locustreader.fragments.SavedArticlesFragment;
import com.tangible.locustreader.interfaces.OnLocustChangeListener;
import com.tangible.locustreader.uielements.SlidingTabLayout;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity implements OnLocustChangeListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    //constants
    int updateIntervalInMinutes = 1;
    int updateIntervalInMillis = updateIntervalInMinutes * 60 * 1000;
    float maxDistanceFromInitialQuery = 1000.0f; //in METERS!

    SectionsPagerAdapter mSectionsPagerAdapter;
    SlidingTabLayout tabs;
    ViewPager mViewPager;

    QueryBuilder queryBuilder;
    private GoogleApiClient mGoogleApiClient;

    HomeFragment homeFragment;
    ArticleMapFragment articleMapFragment;
    SavedArticlesFragment savedArticlesFragment;

    Location locationAtTimeOfQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        homeFragment = new HomeFragment();
        articleMapFragment = new ArticleMapFragment();
        savedArticlesFragment = new SavedArticlesFragment();

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        //tabs setup
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });
        tabs.setViewPager(mViewPager);

        //apis
        buildGoogleApiClient();

        ImageLoader.getInstance().init(new ImageLoaderConfiguration.Builder(this).memoryCache(new LruMemoryCache(10 * 1024 * 1024)).build());
    }

    public Location getLocationAtTimeOfQuery() {
        if (locationAtTimeOfQuery == null) {
            Log.i("HA", "Location Null at getLocationAtTimeOfQuery() !");
        }
        return locationAtTimeOfQuery;
    }

    private void refreshArticleList(double x, double y) {
        Log.i("HA", "Loction recieved, Refreshing article list");
        //invalidate
        // new QueryBuilder(this).doQueryWithCoordinates(x, y);
        new QueryBuilder(this).doInitialRefresh(x, y);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // LAYOUT AND LOADING UPDATES

    @Override
    public void onLayoutComplete(ArrayList<Article> articles) {
        Log.i("HA", "Layout Complete");
        homeFragment.onLayoutComplete(articles);
        homeFragment.onLayoutStatusChange(QueryBuilder.COMPLETE);
        articleMapFragment.onLayoutComplete(articles);
        articleMapFragment.onLayoutStatusChange(QueryBuilder.COMPLETE);
    }

    @Override
    public void onLayoutStatusChange(int status) {
        homeFragment.onLayoutStatusChange(status);
        articleMapFragment.onLayoutStatusChange(status);
    }

    @Override
    public void onProgressPercentChange(String percentageToShow) {
        homeFragment.onProgressPercentChange(percentageToShow);
        articleMapFragment.onProgressPercentChange(percentageToShow);
    }


    // LOCATION STUFF

    @Override
    public void onConnected(Bundle bundle) {
        Log.i("HA", "CONNECTED to Google API Client");
        locationAtTimeOfQuery = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (locationAtTimeOfQuery != null) {
            refreshArticleList(locationAtTimeOfQuery.getLongitude(), locationAtTimeOfQuery.getLatitude());

            //updates
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(updateIntervalInMillis);
            mLocationRequest.setFastestInterval(updateIntervalInMillis + 1);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("HA", "Google API Client connection SUSPENDED");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i("HA", "Google API Client CONNECTION FAILED");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("HA", "Location updated: " + location.getLatitude() + ":" + location.getLatitude() + " (lat:long)\n" +
                "Distance from query location is " + location.distanceTo(locationAtTimeOfQuery) + "m");

        if (location.distanceTo(locationAtTimeOfQuery) > maxDistanceFromInitialQuery) { //Ask if user wants refetch
            //context menu or something
            Log.i("HA", "Location change was sufficient for re-query: delta=" + location.distanceTo(locationAtTimeOfQuery) + "km");

        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            switch (position) {
                case 0: {
                    return homeFragment;
                }
                case 1: {
                    return articleMapFragment; //map fragment
                }
                case 2: {
                    return new PlaceholderFragment();
                    // return savedArticlesFragment; //saved articles
                }
                default: {
                    return new PlaceholderFragment();
                }

            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "HOME";
                case 1:
                    return "MAP";
                case 2:
                    return "SAVED";
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_home, container, false);
            return rootView;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkPlayServices()) {
            mGoogleApiClient.connect();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Log.i("HA", "Google play: user recoverable error with code " + resultCode);
            } else {
                Log.i("HA", "Google play: Device not supported!");
                finish();
            }
            return false;
        }
        return true;
    }

}
